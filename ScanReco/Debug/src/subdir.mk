################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/DataGenerator.cpp \
../src/HistogramFeature.cpp \
../src/ScanFeature.cpp \
../src/Trainer.cpp 

OBJS += \
./src/DataGenerator.o \
./src/HistogramFeature.o \
./src/ScanFeature.o \
./src/Trainer.o 

CPP_DEPS += \
./src/DataGenerator.d \
./src/HistogramFeature.d \
./src/ScanFeature.d \
./src/Trainer.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I/usr/local/Cellar/opencv/2.4.10.1/include/opencv -I/usr/local/Cellar/opencv/2.4.10.1/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


