/*
 * ScanFeature.h
 *
 *  Created on: Feb 6, 2015
 *      Author: manpreet.singh
 */
#include "headers.h"
#ifndef SCANFEATURE_H_
#define SCANFEATURE_H_


class ScanFeature {
public:
	ScanFeature();
	virtual std::vector<float>  getFeature(cv::Mat mat);
	virtual ~ScanFeature();
};

#endif /* SCANFEATURE_H_ */
