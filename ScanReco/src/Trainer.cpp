#include "headers.h"
#include "DataGenerator.h"
using namespace cv;
using namespace std;
int main( int argc, char** argv )
{

  Mat image;
  std::string pathNeg="/Users/manpreet.singh/Code/Workspaces/Cpp3/ScanReco/images-random/";
  std::string pathPos="/Users/manpreet.singh/Code/Workspaces/Cpp3/ScanReco/images-card/";
  DataGenerator generator;
  Mat pos=generator.generate(pathPos);
  Mat posLabel=Mat::ones(pos.rows,1,CV_32FC1);
  cout << pos.rows << " " << posLabel.rows << endl;
  Mat neg=generator.generate(pathNeg);
  Mat negLabel=Mat::zeros(neg.rows,1,CV_32FC1);
  cout << neg.rows << " " << negLabel.rows << endl;
  pos.push_back(neg);//accumulating pos and neg
  posLabel.push_back(negLabel);
  // Set up SVM's parameters
     CvSVMParams params;
     params.svm_type    = CvSVM::C_SVC;
     params.kernel_type = CvSVM::RBF;
     params.gamma = 3 ;
     params.term_crit   = cv::TermCriteria(
             cv::TermCriteria::EPS ,
             1000, // max number of iterations
             .00001); // min accuracy

     // Train the SVM
     CvSVM SVM;
   //  CvParamGrid CvParamGrid_C(pow(2.0,-5), pow(2.0,15), pow(2.0,2));
   //  CvParamGrid CvParamGrid_gamma(pow(2.0,-15), pow(2.0,3), pow(2.0,2));
     SVM.train_auto(pos, posLabel, Mat(), Mat(), params,3);
     SVM.save("../ScanRecoTest/svm.xml");
     for(int i=0;i < pos.rows;i++)
     std::cout << SVM.predict(pos.row(i)) << endl;
     std::cout << pos.rows;
     std::cout << posLabel.rows;
     return 0;
}
