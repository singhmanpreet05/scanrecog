/*
 * HistogramFeature.h
 *
 *  Created on: Feb 6, 2015
 *      Author: manpreet.singh
 */

#ifndef HISTOGRAMFEATURE_H_
#define HISTOGRAMFEATURE_H_
#include "ScanFeature.h"
class HistogramFeature : public ScanFeature {
public:
	HistogramFeature();
	virtual ~HistogramFeature();
	std::vector<float>  getFeature(cv::Mat mat);
};

#endif /* HISTOGRAMFEATURE_H_ */
