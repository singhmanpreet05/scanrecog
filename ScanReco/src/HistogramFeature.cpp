/*
 * HistogramFeature.cpp
 *
 *  Created on: Feb 6, 2015
 *      Author: manpreet.singh
 */

#include "HistogramFeature.h"

HistogramFeature::HistogramFeature() {
	// TODO Auto-generated constructor stub

}
std::vector<float> HistogramFeature::getFeature(cv::Mat src){
	 cv::Mat hsv;
	 cvtColor(src, hsv, CV_BGR2HSV);

	    // Quantize the hue to 30 levels
	    // and the saturation to 32 levels
	    int hbins = 2, sbins = 2;
	    int histSize[] = {hbins, sbins};
//	    int histSize[]={hbins};
	    // hue varies from 0 to 179, see cvtColor
	    float hranges[] = { 0, 180 };
	    // saturation varies from 0 (black-gray-white) to
	    // 255 (pure spectrum color)
	    float sranges[] = { 0, 256 };
	    const float* ranges[] = { hranges, sranges };
//	    const float* ranges[] = {hranges};
	    cv::Mat hist;
	    // we compute the histogram from the 0-th and 1-st channels
	    int channels[] = {0, 1};
//	    int channels[]={0};
	    calcHist( &hsv, 1, channels, cv::Mat(), // do not use mask
	             hist, 2, histSize, ranges,
	             true, // the histogram is uniform
	             false );
//	    double maxVal=0;
//	    minMaxLoc(hist, 0, &maxVal, 0, 0);
	    cv::normalize(hist,hist,0,1,cv::NORM_MINMAX,-1,cv::Mat());
	    std::vector<float>myvector;
	    myvector.assign((float*)hist.datastart,(float*)hist.dataend);
//	    std::cout << myvector.size();
//	    for(int i=0;i < myvector.size();i++){
//	    	std::cout << myvector[i] << " ";
//	    }
//	    std:: cout << std::endl;
	  //  std::cout << result;
	    return myvector;
}
HistogramFeature::~HistogramFeature() {
	// TODO Auto-generated destructor stub
}
