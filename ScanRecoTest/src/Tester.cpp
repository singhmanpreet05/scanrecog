#include "headers.h"
#include "DataGenerator.h"
using namespace std;
using namespace cv;
int main(){
	CvSVM svm;
	svm.load("svm65.xml");
	DataGenerator generator;
//	std::string path="/Users/manpreet.singh/Code/Workspaces/Cpp3/ScanRecoTest/Test";
	std::string path="Test/";
	Mat testData=generator.generate(path);
	for(int i=0;i < testData.rows;i++)
	std::cout << svm.predict(testData.row(i)) << endl;
}
