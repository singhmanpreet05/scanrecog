/*
 * headers.h
 *
 *  Created on: Feb 6, 2015
 *      Author: manpreet.singh
 */

#ifndef HEADERS_H_
#define HEADERS_H_
#include <highgui.h>
#include <cv.h>
#include <iostream>
#include <vector>
#include<dirent.h>
#include<string>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/ml/ml.hpp>
#endif /* HEADERS_H_ */
