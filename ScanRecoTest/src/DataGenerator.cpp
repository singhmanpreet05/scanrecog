/*
 * DataGenertor.cpp
 *
 *  Created on: Feb 8, 2015
 *      Author: manpreet.singh
 */

#include "DataGenerator.h"
using namespace std;
using namespace cv;
DataGenerator::DataGenerator() {
	featureList.push_back(new HistogramFeature());
}
cv::Mat DataGenerator::generate(std::string pathname){
	  DIR *dir=opendir(pathname.c_str());
	  struct dirent *ent;
	  cv::Mat output;
	  if(dir !=NULL){
		  while ((ent = readdir (dir)) != NULL) {
			  	  	  if(strcmp(ent->d_name,".")==0 ||strcmp(ent->d_name,"..")==0 )continue;
			  	  	//  cout << "inside" << endl;
		              string imgName=ent->d_name;
		              cout << imgName << endl;
		              string fullpath=pathname+imgName;
		              Mat image = imread(fullpath);
		              Mat colMat;
		              for(int i=0;i < featureList.size();i++){
		            	  colMat.push_back(Mat(featureList[i]->getFeature(image)));
		              }
		              output.push_back(Mat(colMat.t()));
		              colMat.release();
		              image.release();
		  }
		  closedir (dir);
	  }
	  return output;
}
void DataGenerator::printVector(vector<float> v){
	for(int i=0;i < v.size();i++){
		cout << v[i] << ",";
	}
	cout << endl;
}
DataGenerator::~DataGenerator() {
	// TODO Auto-generated destructor stub
}
