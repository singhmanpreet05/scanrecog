/*
 * DataGenertor.h
 *
 *  Created on: Feb 8, 2015
 *      Author: manpreet.singh
 */

#ifndef DATAGENERATOR_H_
#define DATAGENERATOR_H_
#include "headers.h"
#include "HistogramFeature.h"
#include "ScanFeature.h"
class DataGenerator {
private:
	std::vector<ScanFeature *> featureList;
	void printVector(std::vector<float>v);
public:
	DataGenerator();
	virtual ~DataGenerator();
	cv::Mat generate(std::string pathname);
};

#endif /* DATAGENERATOR_H_ */
