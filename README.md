# README #

This README would documents whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Health Insurance Card Recognition
* 1.0
* Currently the only feature supported are Histograms but the framework is flexible to add any number of features     without changing the training part.

### How do I get set up? ###

ScanReco project is for  training to differentiate between health insurance card images (images-card) and random images (images-random) and creating classifiers.

ScanRecoTest project is for predicting whether the images in test folder are of health insurance cards or not using previously trained classifier.

Follow the following steps to install opencv 
1) https://jjyap.wordpress.com/2014/05/24/installing-opencv-2-4-9-on-mac-osx-with-python-support/
2) Complete the configuration mentioned in step 8

### Results ###
For this simplistic test set the classifier is giving 85% results.
For a complicated test set the results would be far lower than this.

### Who do I talk to? ###
* singh.manpreet05@gmail.com